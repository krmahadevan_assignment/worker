package com.rationaleemotions.worker.pojos;

public record NodeStatus(boolean free) {
}
