package com.rationaleemotions.worker.pojos;

public record IngestionResponse(IngestionAcknowledgement status) {
}
