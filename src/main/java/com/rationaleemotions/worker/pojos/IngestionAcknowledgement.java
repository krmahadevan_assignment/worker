package com.rationaleemotions.worker.pojos;

public enum IngestionAcknowledgement {
    Accepted,
    Rejected
}
