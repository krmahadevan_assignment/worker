package com.rationaleemotions.worker.pojos;

public record IngestionRequest(long jobId, long from, long to, long records) {
}
