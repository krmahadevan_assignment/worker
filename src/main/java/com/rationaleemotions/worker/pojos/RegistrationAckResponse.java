package com.rationaleemotions.worker.pojos;

public record RegistrationAckResponse(String message) {
}
