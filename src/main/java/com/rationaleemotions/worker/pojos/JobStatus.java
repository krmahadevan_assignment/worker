package com.rationaleemotions.worker.pojos;

public enum JobStatus {

    Aborted,
    Submitted,
    Processing,
    Success,
    Failed;

    public static boolean isCompleted(JobStatus status) {
        return status == Success || status == Failed;
    }

}
