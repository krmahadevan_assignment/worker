package com.rationaleemotions.worker.pojos;

public record RegistrationRequest(String ip, int port) {
}
