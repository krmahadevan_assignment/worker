package com.rationaleemotions.worker.pojos;

public record JobStatusUpdateRequest(long jobId, JobStatus status, String fileLocation) {
}
