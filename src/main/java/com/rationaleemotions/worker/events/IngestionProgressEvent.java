package com.rationaleemotions.worker.events;

import com.rationaleemotions.worker.pojos.JobStatus;

import java.util.Objects;

public record IngestionProgressEvent(int index, long jobId, JobStatus status, String fileLocation) {

    public IngestionProgressEvent(int index, long jobId, JobStatus status) {
        this(index, jobId, status, "");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IngestionProgressEvent that = (IngestionProgressEvent) o;
        return jobId == that.jobId;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(jobId);
    }
}
