package com.rationaleemotions.worker.events;

import com.rationaleemotions.worker.internal.GlobalState;
import com.rationaleemotions.worker.pojos.JobStatus;
import com.rationaleemotions.worker.pojos.JobStatusUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
@Slf4j
@EnableScheduling
public class ProgressReportingListener {

    private final RestTemplate restTemplate;
    private final GlobalState globalState;
    @Value("${application.manager.url}")
    String managerUrl;

    private volatile JobStatusUpdateRequest jobStatus;

    @Scheduled(fixedRateString = "${application.statusPollingTimeInSeconds}",
            timeUnit = TimeUnit.SECONDS, scheduler = "basic-scheduler")
    public void postInterimIngestionStatus() {
        if (jobStatus != null) {
            post();
        } else {
            log.info("No Ingestion is currently in progress");
        }
    }

    @EventListener
    public void ingestionProgress(IngestionProgressEvent event) {
        jobStatus = new JobStatusUpdateRequest(event.jobId(), event.status(), event.fileLocation());
        if (JobStatus.isCompleted(jobStatus.status())) {
            post();
            jobStatus = null;
            globalState.markFree();
            log.info("[{}] Completed Job : {}", event.index(), event.jobId());
        } else {
            log.debug("[{}] Job : {} is Still Running", event.index(), event.jobId());
        }
    }

    private void post() {
        if (jobStatus == null) {
            log.info("Job Status was found to be null. Skipping posting of job");
            return;
        }
        if (JobStatus.isCompleted(jobStatus.status())) {
            log.info("Posting final progress of Job {} to {}", jobStatus, managerUrl);
        } else {
            log.info("Posting interim progress of Job {} to {}", jobStatus, managerUrl);
        }
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            // Set request body
            HttpEntity<JobStatusUpdateRequest> requestEntity = new HttpEntity<>(jobStatus, headers);
            String url = managerUrl + "/v1/api/job-status";
            ResponseEntity<String> reply = restTemplate.postForEntity(url, requestEntity, String.class);
            if (!reply.getStatusCode().is2xxSuccessful()) {
                log.info("Error when posting status of Job-ID {} to {}: Return Code {}", jobStatus.jobId(),
                        managerUrl, reply.getStatusCode());
            }
        } catch (Throwable t) {
            log.error("Error while posting job status to {}", jobStatus, t);
        }
    }
}
