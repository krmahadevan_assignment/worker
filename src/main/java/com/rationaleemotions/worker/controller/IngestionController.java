package com.rationaleemotions.worker.controller;

import com.rationaleemotions.worker.pojos.IngestionRequest;
import com.rationaleemotions.worker.pojos.IngestionResponse;
import com.rationaleemotions.worker.pojos.IngestionAcknowledgement;
import com.rationaleemotions.worker.service.IngestionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/api")
@RequiredArgsConstructor
public class IngestionController {

    private final IngestionService ingestionService;

    @PostMapping(value = "/job/submit",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IngestionResponse> ingest(@RequestBody IngestionRequest request) {
        IngestionAcknowledgement status = ingestionService.process(request);
        IngestionResponse response = new IngestionResponse(status);
        if (status == IngestionAcknowledgement.Rejected) {
            return ResponseEntity.unprocessableEntity().body(response);
        }
        return ResponseEntity.ok(response);
    }
}
