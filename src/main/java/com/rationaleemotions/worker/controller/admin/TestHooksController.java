package com.rationaleemotions.worker.controller.admin;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.rationaleemotions.worker.internal.GlobalState;
import com.rationaleemotions.worker.internal.Utils;
import com.rationaleemotions.worker.pojos.NodeStatus;
import com.rationaleemotions.worker.pojos.RegistrationAckResponse;
import com.rationaleemotions.worker.service.S3Service;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/admin")
@RequiredArgsConstructor
public class TestHooksController {

    private final Utils utils;
    private final S3Service s3Service;
    private final GlobalState state;

    @GetMapping("/register")
    public ResponseEntity<RegistrationAckResponse> sendRegistration() {
        ResponseEntity<Void> entity = utils.registrationRequest().retrieve().toBodilessEntity();
        if (entity.getStatusCode().is2xxSuccessful()) {
            return ResponseEntity.ok(new RegistrationAckResponse("Success"));
        }
        return ResponseEntity.unprocessableEntity()
                .body(new RegistrationAckResponse("Registration failed. Received Response code of " + entity.getStatusCode()));
    }

    @GetMapping(value = "/list-s3-buckets", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<S3ObjectSummary>> listS3Buckets() {
        List<S3ObjectSummary> result = s3Service.listObjects();
        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/is-free", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<NodeStatus> isFree() {
        return ResponseEntity.ok(new NodeStatus(state.isFree()));
    }

    @PostMapping(value = "/mark-free")
    ResponseEntity<NodeStatus> markFree() {
        state.markFree();
        return ResponseEntity.ok(new NodeStatus(true));
    }
}
