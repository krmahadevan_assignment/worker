package com.rationaleemotions.worker.internal;

import com.rationaleemotions.worker.pojos.RegistrationRequest;
import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Component
@RequiredArgsConstructor
@Slf4j
public final class Utils {

    private final RestClient client;

    @Value("${server.port}")
    private int port;
    @Value("${application.manager.url}")
    private String managerUrl;

    @Value("${application.pseudo-hostname}")
    private String pseudoHostname;

    @Getter
    private String hostAddress = "127.0.0.1";

    @PostConstruct
    public void init() {
        try {
            InetAddress localHost = InetAddress.getLocalHost();
            if (pseudoHostname != null && !pseudoHostname.isEmpty()) {
                hostAddress = pseudoHostname;
            } else {
                hostAddress = localHost.getHostAddress();
            }
        } catch (UnknownHostException e) {
            log.warn("Unable to resolve host address {}", e.getMessage());
        }
    }

    public RestClient.RequestBodySpec registrationRequest() {
        String url = managerUrl + "/v1/api/register";
        RegistrationRequest payload = new RegistrationRequest(hostAddress, port);
        log.info("Sending registration to {} with payload {}", url, payload);
        return client.post()
                .uri(url)
                .body(payload);
    }

}
