package com.rationaleemotions.worker.internal;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
@Slf4j
public class GlobalState {
    private final AtomicBoolean canAcceptTasks = new AtomicBoolean(true);

    public void markBusy() {
        log.info("Marking busy");
        canAcceptTasks.set(false);
    }

    public void markFree() {
        log.info("Marking free");
        canAcceptTasks.set(true);
    }

    public boolean isFree() {
        return canAcceptTasks.get();
    }
}
