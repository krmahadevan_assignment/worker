package com.rationaleemotions.worker.internal;

import java.io.Closeable;
import java.util.concurrent.locks.ReentrantLock;

public final class AutoCloseableLock implements Closeable {

    private final ReentrantLock internalLock = new ReentrantLock();

    public AutoCloseableLock lock() {
        internalLock.lock();
        return this;
    }

    public boolean isHeldByCurrentThread() {
        return internalLock.isHeldByCurrentThread();
    }

    @Override
    public void close() {
        internalLock.unlock();
    }

}
