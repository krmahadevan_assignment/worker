package com.rationaleemotions.worker.internal;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonWriter;
import com.rationaleemotions.worker.entity.ThirdPartyTable;
import com.rationaleemotions.worker.events.IngestionProgressEvent;
import com.rationaleemotions.worker.pojos.IngestionRequest;
import com.rationaleemotions.worker.pojos.JobStatus;
import com.rationaleemotions.worker.repository.ThirdPartyTableRepository;
import com.rationaleemotions.worker.service.S3Service;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

@Builder
@Slf4j
public class IngestionTask implements Callable<Void> {

    private final int processingTimePerRecordInMs;
    private final IngestionRequest request;
    private final ThirdPartyTableRepository repository;
    private final ApplicationEventPublisher eventPublisher;
    private final S3Service s3Service;
    private final Gson gson = new Gson();

    @Override
    public Void call() {
        try {
            List<ThirdPartyTable> found = repository.findByIdBetween(request.from(), request.to());
            IngestionProgressEvent finalEvent = new IngestionProgressEvent(found.size(), request.jobId(), JobStatus.Failed);
            if (!found.isEmpty()) {
                try {
                    File file = Files.createTempFile("ingestion", "json").toFile();
                    JsonWriter writer = new JsonWriter(new FileWriter(file));
                    writer.beginArray();
                    int index = 1;
                    for (ThirdPartyTable each : found) {
                        //Convert to Json
                        JsonObject json = gson.toJsonTree(each).getAsJsonObject();
                        //Write to file
                        gson.toJson(json, writer);
                        //Publish progress
                        eventPublisher.publishEvent(new IngestionProgressEvent(index, request.jobId(), JobStatus.Processing));
                        //Simulate the delay between processing each records
                        TimeUnit.MILLISECONDS.sleep(processingTimePerRecordInMs);
                        index += 1;
                    }
                    writer.endArray();
                    writer.close();
                    String fileLocation = s3Service.uploadFile(file.getAbsolutePath(), "JobId" + request.jobId());
                    finalEvent = new IngestionProgressEvent(found.size(), request.jobId(), JobStatus.Success, fileLocation);
                } catch (Exception e) {
                    log.warn("Ingestion failed for job {}", request.jobId(), e);
                }

            }
            eventPublisher.publishEvent(finalEvent);
            log.info("Ingestion finished for {}", request.jobId());
        } catch (Exception e) {
            log.warn("Ingestion failed for job {}", request.jobId(), e);
        }
        return null;
    }
}
