package com.rationaleemotions.worker.internal;

import com.rationaleemotions.worker.pojos.RegistrationRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;

import java.util.Timer;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
@RequiredArgsConstructor
@EnableScheduling
public class HeartBeats {

    private final RestClient client;
    private final Utils utils;

    private final Timer timer = new Timer();

    @Value("${server.port}")
    private int port;

    @Value("${application.heartBeatIntervalInSeconds}")
    private int heartBeatIntervalInSeconds;

    @EventListener
    public void onApplicationReady(ApplicationReadyEvent ignored) throws InterruptedException {
        for (int i = 0; i < 3; i++) {
            try {
                log.info("Sending Registration Request to Manager");
                ResponseEntity<Void> entity = utils.registrationRequest().retrieve().toBodilessEntity();
                if (entity.getStatusCode().is2xxSuccessful()) {
                    log.info("Registration Request successful. Status {}", entity.getStatusCode());
                } else {
                    log.info("Registration Request failed. Status: {}", entity.getStatusCode());
                }
                break;
            } catch (Exception e) {
                log.info("Registration Request failed on attempt {}. Cause {}", i, e.getMessage(), e);
                log.info("Waiting for 10 seconds for the manager to be absolutely available.");
                TimeUnit.SECONDS.sleep(10);
            }
        }
    }

    @Scheduled(fixedRateString = "${application.heartBeatIntervalInSeconds}", timeUnit = TimeUnit.SECONDS)
    public void sendHeartBeats() {
        ResponseEntity<Void> response = client.post()
                .uri("/v1/api/heartbeat")
                .header("Content-Type", "application/json")
                .body(new RegistrationRequest(utils.getHostAddress(), port))
                .retrieve()
                .toBodilessEntity();
        log.debug("HeartBeat Response Code: {}", response.getStatusCode().value());
    }
}
