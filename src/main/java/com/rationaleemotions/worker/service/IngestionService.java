package com.rationaleemotions.worker.service;

import com.rationaleemotions.worker.internal.GlobalState;
import com.rationaleemotions.worker.internal.IngestionTask;
import com.rationaleemotions.worker.pojos.IngestionRequest;
import com.rationaleemotions.worker.pojos.IngestionAcknowledgement;
import com.rationaleemotions.worker.repository.ThirdPartyTableRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.concurrent.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class IngestionService {

    @Value("${application.processingTimePerRecordInMs}")
    private int processingTimePerRecordInMs;

    private final ThirdPartyTableRepository repository;
    private final ApplicationEventPublisher publisher;
    private final S3Service s3Service;
    private final GlobalState globalState;

    private final ExecutorService service = Executors.newSingleThreadExecutor();

    public IngestionAcknowledgement process(IngestionRequest request) {
        log.info("Received request {}", request);

        if (!globalState.isFree()) {
            //We already have a task that is running. Cannot accept another task
            log.info("Declined request {}", request);
            return IngestionAcknowledgement.Rejected;
        }
        IngestionTask task = IngestionTask.builder()
                .request(request)
                .s3Service(s3Service)
                .repository(repository)
                .processingTimePerRecordInMs(processingTimePerRecordInMs)
                .eventPublisher(publisher)
                .build();
        service.submit(task);
        globalState.markBusy();
        log.info("Accepted request {}", request);
        return IngestionAcknowledgement.Accepted;
    }
}
