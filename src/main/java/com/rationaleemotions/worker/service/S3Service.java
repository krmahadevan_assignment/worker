package com.rationaleemotions.worker.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class S3Service {

    private final AmazonS3 amazonS3;
    @Value("${aws.bucketName}")
    private String bucketName;
    @Value("${aws.s3.endpoint}")
    private String s3Endpoint;

    public String uploadFile(String localFile, String keyName) {
        File file = new File(localFile);
        amazonS3.putObject(bucketName, keyName, file);
        String location = String.format("%s/%s/%s/%s", s3Endpoint, bucketName, keyName, file.getName());;
        log.info("Uploaded file to S3 location {}", location);
        return location;
    }

    public List<S3ObjectSummary> listObjects() {
        log.info("Retrieving object summaries for bucket '{}'", bucketName);
        ObjectListing objectListing = amazonS3.listObjects(bucketName);
        return objectListing.getObjectSummaries();
    }
}
