package com.rationaleemotions.worker.configs;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.SimpleAsyncTaskScheduler;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;

@Configuration
@Slf4j
public class Configs {


    @Value("${application.manager.url}")
    String managerUrl;

    @Value("${application.useProxyServer}")
    private boolean useProxy;

    @Value("${application.proxyServer.host}")
    private String proxyHost;

    @Value("${application.proxyServer.port}")
    private int proxyPort;

    @Bean
    public RestTemplate restTemplate(ResponseErrorHandler errorHandler) {
        RestTemplateBuilder builder = new RestTemplateBuilder()
                .errorHandler(errorHandler);
        if (useProxy) {
            SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
            SocketAddress address = new InetSocketAddress(proxyHost, proxyPort);
            Proxy proxy = new Proxy(Proxy.Type.HTTP, address);
            requestFactory.setProxy(proxy);
            return builder.requestFactory(() -> requestFactory).build();
        }
        return builder.build();
    }

    @Bean("basic-scheduler")
    public TaskScheduler scheduler() {
        return new SimpleAsyncTaskScheduler();
    }

    @Bean
    public ResponseErrorHandler responseErrorHandler() {
        return new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse reply) throws IOException {
                return reply.getStatusCode().is5xxServerError() ||
                        reply.getStatusCode().is4xxClientError();
            }

            @Override
            public void handleError(ClientHttpResponse reply) throws IOException {
                log.warn("Response error: {} {}", reply.getStatusCode(), reply.getStatusText());
            }
        };
    }

    @Bean
    public RestClient restClient() {
        RestClient.Builder builder = RestClient.builder();
        if (useProxy) {
            SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
            SocketAddress address = new InetSocketAddress(proxyHost, proxyPort);
            Proxy proxy = new Proxy(Proxy.Type.HTTP, address);
            requestFactory.setProxy(proxy);
            builder = builder.requestFactory(requestFactory);
        }

        return builder.baseUrl(managerUrl).build();
    }
}
