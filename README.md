# Worker service

This repo builds a very basic data ingestion worker server.

## Pre-requisites

* Maven
* JDK17
* Docker (builds generate a docker image as well)

## Building the code

```shell
mvn clean package -Dmaven.test.skip=true
```

## Running the server

* First download [this docker-compose file](https://gitlab.com/krmahadevan_assignment/environment/-/blob/main/docker-compose-env.yaml?ref_type=heads)
* Bring up the environment by running:

```shell
docker-compose -f docker-compose-infra.yaml up
```

* Start the server by running

```shell
mvn clean test-compile spring-boot:run
```
This command brings up an app server listening on `5555`.

Following commands are supported by this app server.

```http request
### Used to trigger a Registration request explicitly. To be used by tests ONLY.

GET http://localhost:5555/v1/admin/register HTTP/1.1

### Used to check if node is free. To be used by tests ONLY

GET http://localhost:5555/v1/admin/is-free HTTP/1.1

### Used to mark a node as free. To be used by tests ONLY

POST http://localhost:5555/v1/admin/mark-free HTTP/1.1

### Used to trigger data ingestion

POST http://localhost:5555/v1/api/job/submit HTTP/1.1
Content-Type: application/json

{
  "jobId": 1,
  "from": 1,
  "to": 100,
  "records": 100
}

### List S3 Objects

GET http://localhost:5555/v1/admin/list-s3-buckets HTTP/1.1
```